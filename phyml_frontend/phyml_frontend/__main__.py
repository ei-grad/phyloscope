#!/usr/bin/env python

from os import environ as env
import os
from uuid import uuid4, UUID
import json
from io import StringIO

from tornado.process import Subprocess
from tornado import ioloop, web, gen, iostream

import shortuuid

from Bio import SeqIO

SITE_TITLE = env.get('SITE_TITLE', 'Phylogenetic Docker')
DATA_DIR = env.get('DATA_DIR', '/data')
DEBUG = (env.get('DEBUG', 'False')[0].lower() == 't')
HOSTFILE = env.get("HOSTFILE")

PORT = int(env.get('PORT', 8888))


class MainHandler(web.RequestHandler):
    def get(self):
        self.render("index.html",
                    title=SITE_TITLE)


class Phyml(web.RequestHandler):

    def post(self):
        uuid = str(uuid4())
        ioloop.IOLoop.instance().add_callback(
            phyml, uuid, self.get_argument('contents'), self.get_argument('format', 'phylip')
        )
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({
           "status_url": "/phyml/%s/status" % uuid,
           "result_url": "/phyml/%s/result" % uuid
        }))


DATA = {}


def phyml(uuid, data, fmt='phylip'):

    DATA[uuid] = d = {
        'status': 'running',
        'code': None,
        'stdout': [],
        'stderr': [],
        'names': {},
    }

    fname = os.path.join(DATA_DIR, uuid)

    if fmt == 'phylip':
        with open(fname, 'w') as f:
            f.write(data)
    else:
        d['stdout'].append("Converting .fasta to .phylip")
        seq = list(SeqIO.parse(StringIO(data), fmt))
        for i in seq:
            if len(i.id) > 10:
                d['names'][i.name] = shortuuid.ShortUUID().random(length=10)
                d['stdout'].append("%s would be %s" % (i.id, d['names'][i.id]))
                i.id = d['names'][i.id]
        SeqIO.write(seq, fname, 'phylip')

    del data

    cmd = []

    if HOSTFILE and os.path.exists(HOSTFILE):
        cmd.extend(['mpirun', '--hostfile', '/home/ei-grad/hostfile'])
        d['stdout'].append("Using hostfile: %s" % HOSTFILE)
        d['stdout'].append("Contents:")
        d['stdout'].extend(open(HOSTFILE).read().splitlines())
        d['stdout'].append("")

    cmd.extend(["phyml-mpi", "-i", fname])

    d['stdout'].append("Starting command: %s" % cmd)

    p = Subprocess(
        cmd,
        stdin=Subprocess.STREAM,
        stdout=Subprocess.STREAM,
        stderr=Subprocess.STREAM,
    )

    def finish(ret):
        print("%s finished with %d" % (uuid, ret))
        if ret:
            print(open(os.path.join(DATA_DIR, uuid + '.stdout')).read())
        d['status'] = 'finished'
        d['code'] = ret

    p.set_exit_callback(finish)

    def watcher(stream):

        f = open(os.path.join(DATA_DIR, '%s.%s' % (uuid, stream)), 'wb')

        @gen.coroutine
        def watch():

            try:
                while d['status'] == 'running':
                    s = yield getattr(p, stream).read_until(b'\n')
                    f.write(s)
                    f.flush()
                    d[stream].append(s.decode('utf-8').strip())
            except iostream.StreamClosedError:
                pass

            f.close()

        return watch

    ioloop.IOLoop.instance().add_callback(watcher('stdout'))
    ioloop.IOLoop.instance().add_callback(watcher('stderr'))


def check_valid_uuid(uuid):
    try:
        UUID(uuid)
    except:
        raise web.HTTPError(400, "%s is not valid UUID!" % uuid)


class Status(web.RequestHandler):
    def get(self, uuid):
        if uuid not in DATA:
            raise web.HTTPError(404)
        check_valid_uuid(uuid)
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(DATA[uuid]))


class Result(web.RequestHandler):
    def get(self, uuid):
        # XXX: rewrite for async!
        check_valid_uuid(uuid)
        fname = os.path.join(DATA_DIR, '%s_phyml_tree' % uuid)
        if not os.path.exists(fname):
            raise web.HTTPError(404)
        d = open(fname).read()
        print(d)
        for k, v in DATA[uuid]['names'].items():
            for i in '()|,:;.':
                k = k.replace(i, '_')
            print("Restore %s -> %s" % (v, k))
            d = d.replace(v, k)
        print(d)
        self.write(d)


settings = dict(
    debug=DEBUG,
    static_path=os.path.join(os.path.dirname(__file__), "static"),
)

application = web.Application([
    (r"/", MainHandler),
    (r"/phyml", Phyml),
    (r"/phyml/([^/]+)/status", Status),
    (r"/phyml/([^/]+)/result", Result)
], **settings)


if __name__ == "__main__":
    application.listen(PORT)
    ioloop.IOLoop.instance().start()
