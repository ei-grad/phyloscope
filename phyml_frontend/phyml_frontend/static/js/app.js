(function() {

function load(data) {

  $("#spinner").hide();

  var newick = Newick.parse(data)
  var newickNodes = []
  function buildNewickNodes(node, callback) {
    newickNodes.push(node)
    if (node.branchset) {
      for (var i=0; i < node.branchset.length; i++) {
        buildNewickNodes(node.branchset[i])
      }
    }
  }
  buildNewickNodes(newick);

  console.log("Parsed newick:", newick);
  
  d3.phylogram.buildRadial('#content', newick, {
    inspectSelector: "#inspect",
  });
}

function endsWith(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function watch(data) {
  $.getJSON(data.status_url).success(function(s) {
    console.log(s);
    $("#stdout").text(s.stdout.join('\n'));
    $("#stderr").text(s.stderr.join('\n'));
    if (s.status === 'running') {
      window.setTimeout(function(){watch(data)}, 1000);
    } else if (s.status === 'finished') {
      $.get(data.result_url).success(load);
    }
  });
}

function processFile(file) {
  $("#welcome").remove();
  console.log("Drag & drop'ed file", file);
  var reader = new FileReader();
  reader.onload = function(evt) {
    if (endsWith(file.name, "_phyml_tree") || endsWith(file.name, ".nwk")) {
      load(evt.target.result);
    } else if (endsWith(file.name, ".phylip") || endsWith(file.name, ".phy")) {
      jQuery.ajax({
        url: 'phyml',
        type: 'POST',
        data: {
          filename: file.name,
          contents: evt.target.result,
          format: 'phylip',
        },
      }).success(function(data) {
        $("#spinner").show();
        watch(data);
      });
    } else if (endsWith(file.name, ".fasta")) {
      jQuery.ajax({
        url: 'phyml',
        type: 'POST',
        data: {
          filename: file.name,
          contents: evt.target.result,
          format: 'fasta',
        },
      }).success(function(data) {
        $("#spinner").show();
        watch(data);
      });
    } else {
      alert("Unknown file type! Expected that filename would be like «*_phyml_tree» or «*.phylip».");
    };
  };
  reader.readAsText(file);
}

d3.select("body").on("drop", function() {
  d3.event.stopPropagation();
  d3.event.preventDefault();
  for(var i=0; i<d3.event.dataTransfer.files.length; i++) {
    processFile(d3.event.dataTransfer.files[i]);
  }
}).on('dragover', function () {
  var ev = d3.event;
  ev.stopPropagation();
  ev.preventDefault();
  ev.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
});

})();
