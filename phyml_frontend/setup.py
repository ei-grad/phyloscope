#!/usr/bin/env python

from distutils.core import setup

setup(name='PhyML-Frontend',
      version='0.0',
      description='Python Distribution Utilities',
      author='Andrew Grigorev',
      author_email='andrew@ei-grad.ru',
      url='https://bitbucket.org/ei-grad/phylogenetic-docker/',
      packages=['phyml_frontend'],
      package_data={'phyml_frontend': [
          'index.html',
          'static/js/*.js',
          'static/css/*.css',
          'static/favicon.ico',
      ]},
      requires=['tornado', 'biopython'])
